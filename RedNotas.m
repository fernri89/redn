clc,clear all;

NotaA='NotaA.wav';
NotaB='NotaB.wav';
NotaC='NotaC.wav';
NotaD='NotaD.wav';

[xA,FsA]=audioread(NotaA);
[xB,FsB]=audioread(NotaB);
[xC,FsC]=audioread(NotaC);
[xD,FsD]=audioread(NotaD);

%EntradaN=[xA,xB,xC,xD];
EntradaN=[xA,xB];

yA=ones(length(xA),1);
yB=2.*ones(length(xB),1);
yC=3.*ones(length(xC),1);
yD=4.*ones(length(xD),1);

%SalidaN=[yA,yB,yC,yD];
SalidaN=[yA,yB];